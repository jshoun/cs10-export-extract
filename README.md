## <span style="color:red;">Project deprecated</span>
The migration export has been moved into the CS10 export processing toolset.

# CS10 Export Extract

## Content Server 10.x -- Summary metadata from XML export

Content Server supports XML export of all content and associated metadata.
The data is comprehensive, but it is also complex and it can be difficult to extract
keys fields needed by other systems.

This project provides a simple XML transformation to extract key document and
email data fields in a CSV format. It uses the SAXON streaming transformation
to provide a fast conversion with minimal memory reqirements.

The transformation is implemented with a runable JAR and a stylesheet.
Minimum runtime requirement is Java 7 or higher. 
To convert a CS10 XML export run the following command:

```
java -jar joost.jar exportdata.xml export-2-csv.stx >metadata.csv
```

The utility requires two input parameters:
1. source XML export filename
2. STX stylesheet name
 
The output can be redirected to a file, piped to another process,
or allowed to stream to the console. The file can be opened in Excel.
Fields with embedded commas are enclosed in double quotes.
Double quotes are stripped from all fields.
